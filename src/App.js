import React, { useEffect, useState } from "react";

function App() {
  const [topAnimes, setTopAnimes] = useState([]);

  const fetchTopanimes = async () => {
    const result = await fetch("https://api.jikan.moe/v3/top/anime/1/tv");

    const animeData = await result.json();

    setTopAnimes(animeData.top);
  };

  useEffect(() => {
    fetchTopanimes();
  }, []);

  if (topAnimes.length > 0) {
    const animeList = [];
    for (let i = 0; i < topAnimes.length; i++) {
      console.log(topAnimes[i]);
      console.log(i);

      animeList.push(
        <div className="col-4">
          <div className="card mb-3" style={{ maxWidth: "540px" }}>
            <div className="row g-0">
              <div className="col-md-6">
                <img
                  src={topAnimes[i].image_url}
                  className="img-fluid"
                  alt=""
                />
              </div>
              <div className="col-md-6">
                <div className="card-body">
                  <p className="card-text">Rank : {topAnimes[i].rank}</p>
                  <p className="card-text">Score : {topAnimes[i].score}</p>
                  <h5 className="card-title">{topAnimes[i].title}</h5>
                  <p className="card-text">
                    Episodes : {topAnimes[i].episodes}
                  </p>
                  <p className="card-text">
                    Start Date : {topAnimes[i].start_date}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div className="container mt-4">
        <div className="row text-center ">
          <h3>Valorant Museum</h3>
        </div>
        <div />
        <div className="row">{animeList}</div>
      </div>
    );
  }
  return <>Loading...</>;
}

export default App;
